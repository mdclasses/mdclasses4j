/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.forms.ext;

import com.gltlab.eightm.mdclasses.model.forms.base.BaseItemSpacing;
import com.gltlab.eightm.mdclasses.model.forms.base.ChildrenAlign;
import com.gltlab.eightm.mdclasses.model.forms.base.ItemsAlignment;
import com.gltlab.eightm.mdclasses.model.forms.base.ShowTitle;
import com.gltlab.eightm.mdclasses.model.forms.path.GeneralDataPath;
import com.gltlab.eightm.mdclasses.model.v8enum.CurrentRowUse;
import com.gltlab.eightm.mdclasses.model.v8enum.FormChildrenWidth;
import com.gltlab.eightm.mdclasses.model.v8enum.Group;
import com.gltlab.eightm.mdclasses.model.v8enum.UsualGroupBehavior;
import com.gltlab.eightm.mdclasses.model.v8enum.UsualGroupControlRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.UsualGroupRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.UsualGroupThroughAlign;

import java.util.Map;

public interface UsualGroupExtInfo extends GroupExtInfo, ItemsAlignment, ShowTitle, ChildrenAlign, BaseItemSpacing {
  /**
   * @return Группировка
   */
  Group group();
  UsualGroupBehavior behavior();
  Map<String, String> collapsedRepresentationTitle();
  boolean collapsed();
  UsualGroupControlRepresentation controlRepresentation();
  UsualGroupRepresentation representation();
  boolean showLeftMargin();
  boolean united();
  FormChildrenWidth slaveItemsWidth();
  Map<String, String> format();
  GeneralDataPath titleDataPath();
  UsualGroupThroughAlign throughAlign();
  // Color backColor TODO
  // Color hidden state title back color TODO
  CurrentRowUse currentRowUse();
  int associatedTableElementId();
}
