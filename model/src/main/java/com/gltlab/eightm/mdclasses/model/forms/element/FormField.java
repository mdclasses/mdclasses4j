/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.forms.element;

import com.gltlab.eightm.mdclasses.model.forms.EventHandlerContainer;
import com.gltlab.eightm.mdclasses.model.forms.ExtendedTooltipOwner;
import com.gltlab.eightm.mdclasses.model.forms.TooltipContainer;
import com.gltlab.eightm.mdclasses.model.forms.base.GroupsAlignment;
import com.gltlab.eightm.mdclasses.model.forms.base.ItemsAlignment;
import com.gltlab.eightm.mdclasses.model.forms.base.Shortcut;
import com.gltlab.eightm.mdclasses.model.forms.ext.FieldExtInfo;
import com.gltlab.eightm.mdclasses.model.forms.path.GeneralDataPath;
import com.gltlab.eightm.mdclasses.model.v8enum.FormFixedInTable;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemHorizontalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.ManagedFormFieldType;
import com.gltlab.eightm.mdclasses.model.v8enum.OnMainServerUnavailableBehavior;
import com.gltlab.eightm.mdclasses.model.v8enum.TableFieldEditMode;
import com.gltlab.eightm.mdclasses.model.v8enum.WarningOnEditRepresentation;

import java.util.Map;

public interface FormField extends DataItem, FormStandardCommandSource, TooltipContainer, EventHandlerContainer, ContextMenuOwner, Shortcut, ExtendedTooltipOwner, ItemsAlignment, GroupsAlignment {
  ManagedFormFieldType type();

  void type(ManagedFormFieldType type);

  WarningOnEditRepresentation warningOnEditRepresentation();

  Map<String, String> warningOnEdit();

  TableFieldEditMode editMode();
  void editMode(TableFieldEditMode editMode);

  FormFixedInTable fixingInTable();

  boolean cellHyperlink();

  boolean autoCellHeight();

  boolean showInHeader();
  void showInHeader(boolean showInHeader);

  // Picture headerPicture() TODO
  ItemHorizontalAlignment headerHorizontalAlign();
  void headerHorizontalAlign(ItemHorizontalAlignment headerHorizontalAlign);

  boolean showInFooter();
  void showInFooter(boolean showInFooter);

  GeneralDataPath footerDataPath();

  Map<String, String> footerText();

  //  Color footerTextColor(); TODO
//  Color footerBackColor();
//  Font footerFont();
//  Picture footerPicture();
  ItemHorizontalAlignment footerHorizontalAlign();

  OnMainServerUnavailableBehavior onMainServerUnavailableBehavior();

  FieldExtInfo extInfo();

  void extInfo(FieldExtInfo extInfo);
}
