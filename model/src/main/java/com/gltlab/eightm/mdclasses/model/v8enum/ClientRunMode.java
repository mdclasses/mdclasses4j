/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.v8enum;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum ClientRunMode {
  @XmlEnumValue("Auto") AUTO,
  @XmlEnumValue("ManagedApplication") MANAGED_APPLICATION,
  @XmlEnumValue("OrdinaryApplication") ORDINARY_APPLICATION;

  public static ClientRunMode getClientRunModeByString(String value) {
    return switch (value) {
      case "Auto" -> AUTO;
      case "ManagedApplication" -> MANAGED_APPLICATION;
      case "OrdinaryApplication" -> ORDINARY_APPLICATION;
      default -> throw new IllegalArgumentException("Unexpected ClientRunMode value " + value);
    };
  }
}
