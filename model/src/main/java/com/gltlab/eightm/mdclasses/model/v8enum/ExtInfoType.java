/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.v8enum;

public enum ExtInfoType {
  INPUT_FIELD,
  LABEL_FIELD,
  LABEL_DECORATION,
  USUAL_GROUP,
  COMMAND_BAR,
  FORMATTED_DOC_FIELD,
  PAGE_GROUP,
  PAGES_GROUP,
  COLUMN_GROUP,
  SEARCH_STRING_ADDITION,
  VIEW_STATUS_ADDITION,
  SEARCH_CONTROL_ADDITION,
  BUTTON_GROUP,
  CATALOG_FORM;

  public static ExtInfoType getExtInfoTypeByString(String type) {
    return switch (type) {
      case "form:LabelDecorationExtInfo" -> ExtInfoType.LABEL_DECORATION;
      case "form:LabelFieldExtInfo" -> ExtInfoType.LABEL_FIELD;
      case "form:UsualGroupExtInfo" -> ExtInfoType.USUAL_GROUP;
      case "form:InputFieldExtInfo" -> ExtInfoType.INPUT_FIELD;
      case "form:PageGroupExtInfo" -> ExtInfoType.PAGE_GROUP;
      case "form:PagesGroupExtInfo" -> ExtInfoType.PAGES_GROUP;
      case "form:ColumnGroupExtInfo" -> ExtInfoType.COLUMN_GROUP;
      case "form:SearchStringAdditionExtInfo" -> ExtInfoType.SEARCH_STRING_ADDITION;
      case "form:ViewStatusAdditionExtInfo" -> ExtInfoType.VIEW_STATUS_ADDITION;
      case "form:SearchControlAdditionExtInfo" -> ExtInfoType.SEARCH_CONTROL_ADDITION;
      case "form:CommandBarExtInfo" -> ExtInfoType.COMMAND_BAR;
      case "form:ButtonGroupExtInfo" -> ExtInfoType.BUTTON_GROUP;
      case "form:FormattedDocFieldExtInfo" -> ExtInfoType.FORMATTED_DOC_FIELD;
      case "form:CatalogFormExtInfo" -> ExtInfoType.CATALOG_FORM;
      default -> throw new IllegalArgumentException("Unknown ext info type " + type);
    };
  }
}
