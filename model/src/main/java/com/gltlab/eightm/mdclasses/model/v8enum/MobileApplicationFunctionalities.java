/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.v8enum;

public enum MobileApplicationFunctionalities {
  ADS,
  ALL_FILES_ACCESS,
  APPLICATION_USAGE_STATISTICS,
  AUDIO_PLAYBACK_AND_VIBRATION,
  AUTO_SEND_SMS,
  BACKGROUND_AUDIO_PLAYBACK_AND_VIBRATION,
  BACKGROUND_AUDIO_RECORDING,
  BACKGROUND_LOCATION,
  BARCODE_SCANNING,
  BIOMETRICS,
  BLUETOOTH_PRINTERS,
  CALENDARS,
  CALL_LOG,
  CALL_PROCESSING,
  CAMERA,
  CONTACTS,
  IN_APP_PURCHASES,
  INSTALL_PACKAGES,
  LOCAL_NOTIFICATIONS,
  LOCATION,
  MICROPHONE,
  MUSIC_LIBRARY,
  NUMBER_DIALING,
  OS_BACKUP,
  PERSONAL_COMPUTER_FILE_EXCHANGE,
  PICTURE_AND_VIDEO_LIBRARIES,
  PUSH_NOTIFICATIONS,
  RECEIVE_SMS,
  SMS_LOG,
  WI_FI_PRINTERS;

  public static MobileApplicationFunctionalities getMobileApplicationFunctionalitiesByString(String value) {
    return switch (value) {
      case "Ads" -> ADS;
      case "AllFilesAccess" -> ALL_FILES_ACCESS;
      case "ApplicationUsageStatistics" -> APPLICATION_USAGE_STATISTICS;
      case "AudioPlaybackAndVibration" -> AUDIO_PLAYBACK_AND_VIBRATION;
      case "AutoSendSms" -> AUTO_SEND_SMS;
      case "BackgroundAudioPlaybackAndVibration" -> BACKGROUND_AUDIO_PLAYBACK_AND_VIBRATION;
      case "BackgroundAudioRecording" -> BACKGROUND_AUDIO_RECORDING;
      case "BackgroundLocation" -> BACKGROUND_LOCATION;
      case "BarcodeScanning" -> BARCODE_SCANNING;
      case "Biometrics" -> BIOMETRICS;
      case "BluetoothPrinters" -> BLUETOOTH_PRINTERS;
      case "Calendars" -> CALENDARS;
      case "CallLog" -> CALL_LOG;
      case "CallProcessing" -> CALL_PROCESSING;
      case "Camera" -> CAMERA;
      case "Contacts" -> CONTACTS;
      case "InAppPurchases" -> IN_APP_PURCHASES;
      case "InstallPackages" -> INSTALL_PACKAGES;
      case "LocalNotifications" -> LOCAL_NOTIFICATIONS;
      case "Location" -> LOCATION;
      case "Microphone" -> MICROPHONE;
      case "MusicLibrary" -> MUSIC_LIBRARY;
      case "NumberDialing" -> NUMBER_DIALING;
      case "OSBackup" -> OS_BACKUP;
      case "PersonalComputerFileExchange" -> PERSONAL_COMPUTER_FILE_EXCHANGE;
      case "PictureAndVideoLibraries" -> PICTURE_AND_VIDEO_LIBRARIES;
      case "PushNotifications" -> PUSH_NOTIFICATIONS;
      case "ReceiveSms" -> RECEIVE_SMS;
      case "SmsLog" -> SMS_LOG;
      case "WiFiPrinters" -> WI_FI_PRINTERS;
      default -> throw new IllegalArgumentException("Unknown MobileApplicationFunctionalities value " + value);
    };
  }
}
