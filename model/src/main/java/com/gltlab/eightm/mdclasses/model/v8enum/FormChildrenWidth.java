/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.v8enum;

public enum FormChildrenWidth {
  AUTO,
  EQUAL,
  LEFT_NARROW,
  LEFT_NARROWEST,
  LEFT_WIDE,
  LEFT_WIDEST;

  public static FormChildrenWidth getFormChildrenWidthByString(String value) {
    return switch (value) {
      case "Auto" -> AUTO;
      case "Equal" -> EQUAL;
      case "LeftNarrow" -> LEFT_NARROW;
      case "LeftNarrowest" -> LEFT_NARROWEST;
      case "LeftWide" -> LEFT_WIDE;
      case "LeftWidest" -> LEFT_WIDEST;
      default -> throw new IllegalArgumentException("Unknown FormChildrenWidth value \"" + value + "\"");
    };
  }
}
