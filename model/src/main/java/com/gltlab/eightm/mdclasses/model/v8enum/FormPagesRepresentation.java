/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.v8enum;

public enum FormPagesRepresentation {
  NONE,
  SWIPE,
  TABS_ON_BOTTOM,
  TABS_ON_LEFT_HORIZONTAL,
  TABS_ON_RIGHT_HORIZONTAL,
  TABS_ON_TOP;

  public static FormPagesRepresentation getFormPagesRepresentationByString(String value) {
    return switch (value) {
      case "TabsOnTop" -> TABS_ON_TOP;
      case "None" -> NONE;
      case "TabsOnBottom" -> TABS_ON_BOTTOM;
      case "TabsOnLeftHorizontal" -> TABS_ON_LEFT_HORIZONTAL;
      case "TabsOnRightHorizontal" -> TABS_ON_RIGHT_HORIZONTAL;
      default -> throw new IllegalArgumentException("Unknown FormPagesRepresentation value \"" + value + "\"");
    };
  }
}
