/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.v8enum;

public enum ManagedFormFieldType {
  CALENDAR_FIELD,
  CHART_FIELD,
  CHECK_BOX_FIELD,
  DENDROGRAM_FIELD,
  FORMATTED_DOCUMENT_FIELD,
  GANTT_CHART_FIELD,
  GEOGRAPHICAL_SCHEMA_FIELD,
  GRAPHICAL_SCHEMA_FIELD,
  HTML_DOCUMENT_FIELD,
  INPUT_FIELD,
  LABEL_FIELD,
  NONE,
  PERIOD_FIELD,
  PICTURE_FIELD,
  PLANNER_FIELD,
  PROGRESS_BAR_FIELD,
  RADIO_BUTTON_FIELD,
  SPREADSHEET_DOCUMENT_FIELD,
  TEXT_DOCUMENT_FIELD,
  TRACK_BAR_FIELD;

  public static ManagedFormFieldType getValueByString(String value) {
    return switch (value) {
      case "LabelField" -> LABEL_FIELD;
      case "InputField" -> INPUT_FIELD;
      default -> throw new IllegalArgumentException("Unknown ManagedFormFieldType \"" + value + "\"");
    };
  }
}
