/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.forms.ext;

import com.gltlab.eightm.mdclasses.model.base.Value;
import com.gltlab.eightm.mdclasses.model.forms.Form;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseAutoDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseMaxDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.BaseStretchable;
import com.gltlab.eightm.mdclasses.model.forms.base.Formatted;
import com.gltlab.eightm.mdclasses.model.forms.base.MarkNegatives;
import com.gltlab.eightm.mdclasses.model.forms.base.MinWidthDimension;
import com.gltlab.eightm.mdclasses.model.forms.base.PasswordMode;
import com.gltlab.eightm.mdclasses.model.v8enum.AutoCapitalizationOnTextInput;
import com.gltlab.eightm.mdclasses.model.v8enum.AutoCorrectionOnTextInput;
import com.gltlab.eightm.mdclasses.model.v8enum.AutoShowClearButtonMode;
import com.gltlab.eightm.mdclasses.model.v8enum.AutoShowOpenButtonMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ChoiceButtonRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.ChoiceHistoryOnInput;
import com.gltlab.eightm.mdclasses.model.v8enum.EditTextUpdate;
import com.gltlab.eightm.mdclasses.model.v8enum.FoldersAndItems;
import com.gltlab.eightm.mdclasses.model.v8enum.IncompleteItemChoiceMode;
import com.gltlab.eightm.mdclasses.model.v8enum.LogFormElementHeightControlVariant;
import com.gltlab.eightm.mdclasses.model.v8enum.OnScreenKeyboardReturnKeyText;
import com.gltlab.eightm.mdclasses.model.v8enum.SpecialTextInputMode;
import com.gltlab.eightm.mdclasses.model.v8enum.SpellCheckingOnTextInput;

import java.util.Map;

public interface InputFieldExtInfo extends FieldExtInfo, BaseAutoDimension, BaseStretchable, PasswordMode, BaseDimension,
  BaseMaxDimension, MinWidthDimension, MarkNegatives, Formatted {
  boolean wrap();

  void wrap(boolean wrap);

  boolean multiLine();

  boolean extendedEdit();

  boolean choiceListButton();

  boolean dropListButton();

  boolean choiceButton();

  ChoiceButtonRepresentation choiceButtonRepresentation();

  // Picture choiceButtonPicture TODO
  boolean clearButton();

  boolean spinButton();

  boolean openButton();

  boolean createButton();

  String mask();

  boolean autoChoiceIncomplete();

  boolean quickChoice();

  FoldersAndItems choiceFoldersAndItems();

  Map<String, String> editFormat();

  boolean listChoiceMode();

  int choiceListHeight();

  boolean autoMarkIncomplete();

  boolean chooseType();

  void chooseType(boolean chooseType);

  IncompleteItemChoiceMode incompleteItemChoiceMode();

  boolean typeDomainEnabled();

  void typeDomainEnabled(boolean typeDomainEnabled);

  boolean textEdit();

  void textEdit(boolean textEdit);

  EditTextUpdate editTextUpdate();

  Value minValue();

  Value maxValue();

  Form choiceForm();

  // List<FormChoiceParameterLink> choiceParameterLinks() TODO
  // List<ChoiceParameter> choiceParameters() TODO
  // TypeDescription availableTypes() TODO
  // List<FormChoiceListDesTimeValue> getChoiceList() TODO
  // Text Color
  // Back Color
  // Border Color
  // Font
  // Type Link
  LogFormElementHeightControlVariant heightControlVariant();

  AutoShowClearButtonMode autoShowClearButtonMode();

  AutoShowOpenButtonMode autoShowOpenButtonMode();

  AutoCorrectionOnTextInput autoCorrectionOnTextInput();

  SpellCheckingOnTextInput spellCheckingOnTextInput();

  AutoCapitalizationOnTextInput autoCapitalizationOnTextInput();

  SpecialTextInputMode specialTextInputMode();

  OnScreenKeyboardReturnKeyText onScreenKeyboardReturnKeyText();

  Map<String, String> inputHint();

  int dropListWidth();

  ChoiceHistoryOnInput choiceHistoryOnInput();
}
