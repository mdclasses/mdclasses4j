/*
 * Copyright (c) 2022. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.md.children;

import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.base.ToolTip;
import com.gltlab.eightm.mdclasses.model.base.TypeDescriptionProvider;
import com.gltlab.eightm.mdclasses.model.base.Value;
import com.gltlab.eightm.mdclasses.model.forms.Form;
import com.gltlab.eightm.mdclasses.model.forms.base.Formatted;
import com.gltlab.eightm.mdclasses.model.forms.base.MarkNegatives;
import com.gltlab.eightm.mdclasses.model.forms.base.PasswordMode;
import com.gltlab.eightm.mdclasses.model.forms.base.TypeDescription;
import com.gltlab.eightm.mdclasses.model.md.ConfigurationElement;
import com.gltlab.eightm.mdclasses.model.v8enum.CreateOnInput;
import com.gltlab.eightm.mdclasses.model.v8enum.FillChecking;
import com.gltlab.eightm.mdclasses.model.v8enum.FoldersAndItemsUse;
import com.gltlab.eightm.mdclasses.model.v8enum.UseQuickChoice;

import java.util.List;

public interface BasicFeature extends PasswordMode, Formatted, MarkNegatives, ToolTip, ConfigurationElement, TypeDescriptionProvider {
  TypeDescription type();

  void type(TypeDescription typeDescription);

  MultiLanguageText editFormat();

  void editFormat(MultiLanguageText editFormat);

  String mask();

  void mask(String mask);

  boolean multiLine();

  void multiLine(boolean multiLine);

  boolean extendedEdit();

  void extendedEdit(boolean extendedEdit);

  Value minValue();

  void minValue(Value minValue);

  Value maxValue();

  void maxValue(Value maxValue);

  FillChecking fillChecking();

  void fillChecking(FillChecking fillChecking);

  FoldersAndItemsUse choiceFoldersAndItems();

  void choiceFoldersAndItems(FoldersAndItemsUse choiceFoldersAndItems);

  List<ChoiceParameterLink> choiceParameterLinks();

  List<ChoiceParameter> choiceParameters();

  UseQuickChoice quickChoice();

  void quickChoice(UseQuickChoice quickChoice);

  CreateOnInput createOnInput();

  void createOnInput(CreateOnInput createOnInput);

  Form choiceForm();

  void choiceForm(Form choiceForm);

  TypeLink linkByType();

  void linkByType(TypeLink typeLink);
}
