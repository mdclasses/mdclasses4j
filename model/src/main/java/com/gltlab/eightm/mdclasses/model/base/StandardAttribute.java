/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gltlab.eightm.mdclasses.model.base;

import com.gltlab.eightm.mdclasses.model.forms.TooltipContainer;
import com.gltlab.eightm.mdclasses.model.forms.base.Formatted;
import com.gltlab.eightm.mdclasses.model.forms.base.MarkNegatives;
import com.gltlab.eightm.mdclasses.model.forms.base.PasswordMode;
import com.gltlab.eightm.mdclasses.model.v8enum.FillChecking;
import com.gltlab.eightm.mdclasses.model.v8enum.FullTextSearchUsing;

public interface StandardAttribute extends Nameable, SynonymOwner, Commentable, Formatted, QuickChoice,
  PasswordMode, MarkNegatives, DataHistorySupport {
  boolean fillFromFillingValue();

  void fillFromFillingValue(boolean fillFromFillingValue);

  Value fillValue();

  void fillValue(Value fillValue);

  FillChecking fillChecking();

  void fillChecking(FillChecking fillChecking);

  FullTextSearchUsing fullTextSearch();

  void fullTextSearch(FullTextSearchUsing fullTextSearch);

  String mask();

  void mask(String mask);

  boolean extendedEdit();

  void extendedEdit(boolean extendedEdit);

  boolean multiLine();

  void multiLine(boolean multiLine);

  Value minValue();

  void minValue(Value minValue);

  Value maxValue();

  void maxValue(Value maxValue);

  MultiLanguageText toolTip();

  void toolTip(MultiLanguageText toolTip);
}
