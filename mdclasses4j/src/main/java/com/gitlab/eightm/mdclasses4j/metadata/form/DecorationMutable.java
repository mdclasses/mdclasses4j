/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.form;

import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.base.RoleBoolean;
import com.gltlab.eightm.mdclasses.model.forms.Decoration;
import com.gltlab.eightm.mdclasses.model.forms.EventHandler;
import com.gltlab.eightm.mdclasses.model.forms.ext.DecorationExtInfo;
import com.gltlab.eightm.mdclasses.model.v8enum.DisplayImportance;
import com.gltlab.eightm.mdclasses.model.v8enum.FormDecorationType;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemHorizontalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemVerticalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.TooltipRepresentation;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class DecorationMutable implements Decoration {
  private int id;
  private String name;
  private List<EventHandler> handlers = new ArrayList<>();
  private boolean autoMaxWidth;
  private boolean autoMaxHeight;
  private DecorationExtInfo extInfo;
  private boolean formatted;
  private FormDecorationType decorationType;
  private int maxWidth;
  private int minWidth;
  private int maxHeight;
  private TooltipRepresentation tooltipRepresentation;
  private MultiLanguageText tooltip;
  private int width;
  private int height;
  private boolean enabled;
  private boolean visible;
  private RoleBoolean userVisible;
  private String shortcut;
  private boolean skipOnInput;
  private boolean horizontalStretch;
  private boolean verticalStretch;
  private MultiLanguageText title;
  private DisplayImportance displayImportance;
  private ItemHorizontalAlignment groupHorizontalAlignment;
  private ItemVerticalAlignment groupVerticalAlignment;
}
