/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.form;

import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.base.RoleBoolean;
import com.gltlab.eightm.mdclasses.model.forms.ExtendedTooltip;
import com.gltlab.eightm.mdclasses.model.forms.element.FormGroup;
import com.gltlab.eightm.mdclasses.model.forms.element.FormItem;
import com.gltlab.eightm.mdclasses.model.forms.ext.GroupExtInfo;
import com.gltlab.eightm.mdclasses.model.v8enum.DisplayImportance;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemHorizontalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemVerticalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.ManagedFormGroupType;
import com.gltlab.eightm.mdclasses.model.v8enum.TooltipRepresentation;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class FormGroupMutable implements FormGroup {
  private int id;
  private String name;
  private boolean visible;
  private boolean enabled;
  private RoleBoolean userVisible;
  private List<FormItem> items = new ArrayList<>();
  private MultiLanguageText title;
  private TooltipRepresentation tooltipRepresentation;
  private MultiLanguageText tooltip;
  private GroupExtInfo extInfo;
  private ManagedFormGroupType groupType;
  private DisplayImportance displayImportance;
  private boolean readOnly;
  private boolean horizontalStretch;
  private boolean verticalStretch;
  private ItemHorizontalAlignment groupHorizontalAlignment;
  private ItemVerticalAlignment groupVerticalAlignment;
  private int height;
  private int width;
  private ExtendedTooltip extendedTooltip;
}
