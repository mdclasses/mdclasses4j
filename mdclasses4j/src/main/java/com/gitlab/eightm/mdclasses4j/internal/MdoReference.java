/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.internal;

import com.gitlab.eightm.mdclasses4j.MetadataType;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.regex.Pattern;

@Getter
public class MdoReference {

  private static final Pattern DOT = Pattern.compile("\\.", Pattern.UNICODE_CASE);
  private final MetadataType metadataType;
  private final String name;

  public MdoReference(@NotNull String reference) {
    var referenceParts = DOT.split(reference);
    if (referenceParts.length != 2) {
      throw new IllegalArgumentException("Wrong reference format. Must be Type.Name");
    }

    this.metadataType = MetadataType.getMetadataTypeByString(referenceParts[0]);
    this.name = referenceParts[1];
  }

  public MdoReference(@NotNull MetadataType metadataType, @NotNull String name) {
    this.metadataType = metadataType;
    this.name = name;
  }

  public static String getNameFromReference(String metadataReference) {
    var referenceParts = DOT.split(metadataReference);
    if (referenceParts.length != 2) {
      throw new IllegalArgumentException("Wrong reference format. Must be Type.Name");
    }

    return referenceParts[1];
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MdoReference that = (MdoReference) o;
    return metadataType == that.metadataType && name.equals(that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(metadataType, name);
  }

  @Override
  public String toString() {
    return metadataType + "." + name;
  }
}
