/*
 * Copyright (c) 2022. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.jaxb;

import org.glassfish.jaxb.core.marshaller.CharacterEscapeHandler;

import java.io.IOException;
import java.io.Writer;

public class CustomCharacterEscapeHandler implements CharacterEscapeHandler {

  @Override
  public void escape(char[] ch, int start, int length, boolean isAttVal, Writer out) throws IOException {
    int limit = start+length;
    for (int i = start; i < limit; i++) {
      char c = ch[i];
      if (c == '&' || c == '<' || c == '>' || c == '\r' || (c == '\n' && isAttVal) || (c == '\"')) {
        if (i != start)
          out.write(ch, start, i - start);
        start = i + 1;
        switch (ch[i]) {
          case '&' -> out.write("&amp;");
          case '<' -> out.write("&lt;");
          case '>' -> out.write("&gt;");
          case '\"' -> out.write("&quot;");
          case '\n', '\r' -> {
            out.write("&#");
            out.write(Integer.toString(c));
            out.write(';');
          }
          default -> throw new IllegalArgumentException("Cannot escape: '" + c + "'");
        }
      }
    }

    if( start!=limit )
      out.write(ch,start,limit-start);
  }
}
