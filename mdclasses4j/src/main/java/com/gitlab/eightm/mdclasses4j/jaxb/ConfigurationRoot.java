/*
 * Copyright (c) 2022. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.jaxb;

import com.gitlab.eightm.mdclasses4j.entity.ConfigurationEntity;
import com.gltlab.eightm.mdclasses.model.ContainedObject;
import com.gltlab.eightm.mdclasses.model.base.MultiLanguageContent;
import com.gltlab.eightm.mdclasses.model.v8enum.ApplicationUsePurpose;
import com.gltlab.eightm.mdclasses.model.v8enum.ClientRunMode;
import com.gltlab.eightm.mdclasses.model.v8enum.CompatibilityMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ScriptVariant;

import java.util.Collections;
import java.util.List;

public record ConfigurationRoot(
  String uuid,
  String name,
  List<MultiLanguageContent> synonym,
  List<ContainedObject> containedObjects,
  CompatibilityMode configurationCompatibilityMode,
  ClientRunMode defaultRunMode,
  List<ApplicationUsePurpose> usePurposes,
  ScriptVariant scriptVariant,
  String vendor
) {
  public ConfigurationRoot(ConfigurationEntity configurationEntity) {
    this(
      configurationEntity.uuid(),
      configurationEntity.name(),
      configurationEntity.synonym(),
      Collections.unmodifiableList(configurationEntity.containedObjects()),
      configurationEntity.configurationCompatibilityMode(),
      configurationEntity.defaultRunMode(),
      Collections.unmodifiableList(configurationEntity.usePurposes()),
      configurationEntity.scriptVariant(),
      configurationEntity.vendor()
    );
  }
}
