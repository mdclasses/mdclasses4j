/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.general.children.values;

import com.gltlab.eightm.mdclasses.model.base.Value;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ValueFactory {
  public static Value getValueByStringType(String typeValue) {
    return switch (typeValue) {
      case "core:UndefinedValue" -> new UndefinedValueMutable();
      case "core:BooleanValue" -> new BooleanValueMutable();
      case "core:StringValue" -> new StringValueMutable();
      case "core:ReferenceValue" -> new ReferenceValueMutable();
      default -> throw new IllegalArgumentException("Unknown Value " + typeValue);
    };
  }
}
