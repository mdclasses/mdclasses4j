/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.general.children;

import com.gltlab.eightm.mdclasses.model.base.MultiLanguageContent;
import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.base.StandardAttribute;
import com.gltlab.eightm.mdclasses.model.base.Value;
import com.gltlab.eightm.mdclasses.model.v8enum.DataHistoryUse;
import com.gltlab.eightm.mdclasses.model.v8enum.FillChecking;
import com.gltlab.eightm.mdclasses.model.v8enum.FullTextSearchUsing;
import com.gltlab.eightm.mdclasses.model.v8enum.TooltipRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.UseQuickChoice;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class StandardAttributeMutable implements StandardAttribute {
  private String name;
  private List<MultiLanguageContent> synonym;
  private String comment;
  private UseQuickChoice useQuickChoice;
  private boolean fillFromFillingValue;
  private Value fillValue;
  private FillChecking fillChecking;
  private FullTextSearchUsing fullTextSearch;
  private String mask = "";
  private boolean extendedEdit;
  private boolean multiLine;
  private Value minValue;
  private Value maxValue;
  private MultiLanguageText toolTip;
  private Map<String, String> format = new HashMap<>();
  private boolean markNegatives;
  private boolean passwordMode;
  private DataHistoryUse dataHistoryUse;
}
