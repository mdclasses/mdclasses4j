/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.form.ext;

import com.gltlab.eightm.mdclasses.model.forms.EventHandler;
import com.gltlab.eightm.mdclasses.model.forms.ext.UsualGroupExtInfo;
import com.gltlab.eightm.mdclasses.model.forms.path.GeneralDataPath;
import com.gltlab.eightm.mdclasses.model.v8enum.CurrentRowUse;
import com.gltlab.eightm.mdclasses.model.v8enum.FormChildrenAlign;
import com.gltlab.eightm.mdclasses.model.v8enum.FormChildrenWidth;
import com.gltlab.eightm.mdclasses.model.v8enum.FormItemSpacing;
import com.gltlab.eightm.mdclasses.model.v8enum.Group;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemHorizontalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemVerticalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.UsualGroupBehavior;
import com.gltlab.eightm.mdclasses.model.v8enum.UsualGroupControlRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.UsualGroupRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.UsualGroupThroughAlign;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Setter
@Getter
public class UsualGroupExtInfoMutable implements UsualGroupExtInfo {
  private List<EventHandler> handlers = new ArrayList<>();
  private ItemHorizontalAlignment horizontalAlignment;
  private ItemVerticalAlignment verticalAlignment;
  private Group group;
  private FormChildrenAlign childrenAlign;
  private FormItemSpacing horizontalSpacing;
  private FormItemSpacing verticalSpacing;
  private UsualGroupBehavior behavior;
  private Map<String, String> collapsedRepresentationTitle = new HashMap<>();
  private boolean collapsed;
  private UsualGroupControlRepresentation controlRepresentation;
  private UsualGroupRepresentation representation;
  private boolean showLeftMargin;
  private boolean united;
  private FormChildrenWidth slaveItemsWidth;
  private Map<String, String> format = new HashMap<>();
  private boolean showTitle;
  private GeneralDataPath titleDataPath;
  private UsualGroupThroughAlign throughAlign;
  private CurrentRowUse currentRowUse;
  private int associatedTableElementId;
}
