/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.form.ext;

import com.gltlab.eightm.mdclasses.model.base.Value;
import com.gltlab.eightm.mdclasses.model.forms.EventHandler;
import com.gltlab.eightm.mdclasses.model.forms.Form;
import com.gltlab.eightm.mdclasses.model.forms.ext.InputFieldExtInfo;
import com.gltlab.eightm.mdclasses.model.v8enum.AutoCapitalizationOnTextInput;
import com.gltlab.eightm.mdclasses.model.v8enum.AutoCorrectionOnTextInput;
import com.gltlab.eightm.mdclasses.model.v8enum.AutoShowClearButtonMode;
import com.gltlab.eightm.mdclasses.model.v8enum.AutoShowOpenButtonMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ChoiceButtonRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.ChoiceHistoryOnInput;
import com.gltlab.eightm.mdclasses.model.v8enum.EditTextUpdate;
import com.gltlab.eightm.mdclasses.model.v8enum.FoldersAndItems;
import com.gltlab.eightm.mdclasses.model.v8enum.IncompleteItemChoiceMode;
import com.gltlab.eightm.mdclasses.model.v8enum.LogFormElementHeightControlVariant;
import com.gltlab.eightm.mdclasses.model.v8enum.OnScreenKeyboardReturnKeyText;
import com.gltlab.eightm.mdclasses.model.v8enum.SpecialTextInputMode;
import com.gltlab.eightm.mdclasses.model.v8enum.SpellCheckingOnTextInput;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Setter
@Getter
public class InputFieldExtInfoMutable implements InputFieldExtInfo {
  private List<EventHandler> handlers = new ArrayList<>();
  private boolean autoMaxHeight;
  private boolean autoMaxWidth;
  private int width;
  private int height;
  private int maxWidth;
  private int minWidth;
  private int maxHeight;
  private boolean passwordMode;
  private boolean horizontalStretch;
  private boolean verticalStretch;
  private boolean wrap;
  private boolean multiLine;
  private boolean extendedEdit;
  private boolean markNegatives;
  private boolean choiceListButton;
  private boolean dropListButton;
  private boolean choiceButton;
  private ChoiceButtonRepresentation choiceButtonRepresentation;
  private boolean clearButton;
  private boolean spinButton;
  private boolean openButton;
  private boolean createButton;
  private String mask;
  private boolean autoChoiceIncomplete;
  private boolean quickChoice;
  private FoldersAndItems choiceFoldersAndItems;
  private Map<String, String> format = new HashMap<>();
  private Map<String, String> editFormat = new HashMap<>();
  private boolean listChoiceMode;
  private int choiceListHeight;
  private boolean autoMarkIncomplete;
  private boolean chooseType;
  private IncompleteItemChoiceMode incompleteItemChoiceMode;
  private boolean typeDomainEnabled;
  private boolean textEdit;
  private EditTextUpdate editTextUpdate;
  private Value minValue;
  private Value maxValue;
  private Form choiceForm;
  private LogFormElementHeightControlVariant heightControlVariant;
  private AutoShowClearButtonMode autoShowClearButtonMode;
  private AutoShowOpenButtonMode autoShowOpenButtonMode;
  private AutoCorrectionOnTextInput autoCorrectionOnTextInput;
  private SpellCheckingOnTextInput spellCheckingOnTextInput;
  private AutoCapitalizationOnTextInput autoCapitalizationOnTextInput;
  private SpecialTextInputMode specialTextInputMode;
  private OnScreenKeyboardReturnKeyText onScreenKeyboardReturnKeyText;
  private Map<String, String> inputHint = new HashMap<>();
  private int dropListWidth;
  private ChoiceHistoryOnInput choiceHistoryOnInput;
}
