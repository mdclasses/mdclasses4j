/*
 * Copyright (c) 2022. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.internal;

import com.gitlab.eightm.mdclasses4j.entity.ConfigurationEntity;
import com.gitlab.eightm.mdclasses4j.entity.right.RightsEntity;
import com.gltlab.eightm.mdclasses.model.md.Role;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

@Component
public class ConfigurationParser {
  private final JAXBContext jaxbContext;
  private Path pathToProject;

  public ConfigurationParser(JAXBContext jaxbContext) {
    this.jaxbContext = jaxbContext;
  }

  public void initialize(Path pathToProject) {
    if (this.pathToProject == null) {
      this.pathToProject = pathToProject;
    } else {
      throw new IllegalStateException("Parser must initialize only once.");
    }
  }

  public Optional<ConfigurationEntity> parseConfigurationRoot(@NotNull Path pathToProjectRoot) throws JAXBException {
    var pathToRoot = Path.of(pathToProjectRoot.toString(), "Configuration", "Configuration.mdo");
    if (!Files.exists(pathToRoot)) {
      // TODO log error
      return Optional.empty();
    }

    var unmarshaller = jaxbContext.createUnmarshaller();
    ConfigurationEntity configuration = (ConfigurationEntity) unmarshaller.unmarshal(pathToRoot.toFile());
    return Optional.of(configuration);
  }

  @SneakyThrows
  public @NotNull Role parseRole(@NotNull MdoReference defaultRoleReference) {
    var roleName = defaultRoleReference.name();
    var pathToRoleFolder = Path.of(pathToProject.toString(), "Roles", roleName);
    if (!Files.exists(pathToRoleFolder)) {
      return null;
    }

    var pathToRights = Path.of(pathToRoleFolder.toString(), "Rights.rights");
    var unmarshaller = jaxbContext.createUnmarshaller();
    RightsEntity rightsEntity = (RightsEntity) unmarshaller.unmarshal(pathToRights.toFile());

    return null;
  }
}
