/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.form.ext;

import com.gltlab.eightm.mdclasses.model.forms.EventHandler;
import com.gltlab.eightm.mdclasses.model.forms.ext.PagesGroupExtInfo;
import com.gltlab.eightm.mdclasses.model.v8enum.CurrentRowUse;
import com.gltlab.eightm.mdclasses.model.v8enum.FormPagesRepresentation;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class PagesGroupExtInfoMutable implements PagesGroupExtInfo {
  private List<EventHandler> handlers = new ArrayList<>();
  private FormPagesRepresentation pagesRepresentation;
  private CurrentRowUse currentRowUse;
  private int associatedTableElementId;
}
