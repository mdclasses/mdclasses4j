/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.internal;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ConfigurationTag {
  public final String FORM = "form:Form";
  public final String NAME = "name";
  public final String SYNONYM = "synonym";
  public final String KEY = "key";
  public final String VALUE = "value";
  public final String COMMENT = "comment";
  public final String CONFIGURATION_EXTENSION_COMPATIBILITY_MODE = "configurationExtensionCompatibilityMode";
  public final String DEFAULT_RUN_MODE = "defaultRunMode";
  public final String USE_PURPOSES = "usePurposes";
  public final String SCRIPT_VARIANT = "scriptVariant";
  public final String VENDOR = "vendor";
  public final String VERSION = "version";
  public final String DEFAULT_LANGUAGE = "defaultLanguage";
  public final String BRIEF_INFORMATION = "briefInformation";
  public final String DETAILED_INFORMATION = "detailedInformation";
  public final String DATA_LOCK_CONTROL_MODE = "dataLockControlMode";
  public final String MODALITY_USE_MODE = "modalityUseMode";
  public final String SYNCHRONOUS_PLATFORM_EXTENSION_AND_ADD_IN_CALL_USE_MODE = "synchronousPlatformExtensionAndAddInCallUseMode";
  public final String COMPATIBILITY_MODE = "compatibilityMode";
  // form
  public final String ITEMS = "items";
  public final String VISIBLE = "visible";
  public final String ENABLED = "enabled";
  public final String ID = "id";
  public final String EXTENDED_TOOLTIP = "extendedTooltip";
  public final String HANDLERS = "handlers";
  public final String CONTEXT_MENU = "contextMenu";
  public final String AUTO_COMMAND_BAR = "autoCommandBar";
  public final String USER_VISIBLE = "userVisible";
  public final String AUTO_MAX_WIDTH = "autoMaxWidth";
  public final String AUTO_MAX_HEIGHT = "autoMaxHeight";
  public final String HORIZONTAL_ALIGN = "horizontalAlign";
  public final String COMMAND_NAME = "commandName";
  public final String REPRESENTATION = "representation";
  public final String PLACEMENT_AREA = "placementArea";
  public final String REPRESENTATION_IN_CONTEXT_MENU = "representationInContextMenu";
  public final String DATA_PATH = "dataPath";
  public final String SEGMENTS = "segments";
  public final String TYPE = "type";
  public final String TITLE = "title";
  public final String TOOLTIP = "tooltip";
  public final String COMMON = "common";
  public final String SEARCH_STRING_ADDITION = "searchStringAddition";
  public final String VIEW_STATUS_ADDITION = "viewStatusAddition";
  public final String SEARCH_CONTROL_ADDITION = "searchControlAddition";
  public final String EVENT = "event";
  public final String EXT_INFO = "extInfo";
  public final String ATTRIBUTES = "attributes";
  public final String EDIT = "edit";
  public final String MAIN = "main";
  public final String SAVED_DATA = "savedData";
  public final String VIEW = "view";
  public final String AUTO_FILL = "autoFill";
  public final String EDIT_MODE = "editMode";
  public final String SHOW_IN_HEADER = "showInHeader";
  public final String HEADER_HORIZONTAL_ALIGN = "headerHorizontalAlign";
  public final String SHOW_IN_FOOTER = "showInFooter";
  public final String WRAP = "wrap";
  public final String CHOOSE_TYPE = "chooseType";
  public final String TYPE_DOMAIN_ENABLED = "typeDomainEnabled";
  public final String TEXT_EDIT = "textEdit";
  public final String TITLE_LOCATION = "titleLocation";
  public final String AUTO_INSERT_NEW_ROW = "autoInsertNewRow";
  public final String AUTO_MAX_ROW_COUNT = "autoMaxRowCount";
  public final String CHANGE_ROW_ORDER = "changeRowOrder";
  public static final String CHANGE_ROW_SET = "changeRowSet";
  public static final String ENABLE_DRAG = "enableDrag";
  public static final String ENABLE_START_DRAG = "enableStartDrag";
  public static final String FILE_DRAG_MODE = "fileDragMode";
  public static final String FOOTER_HEIGHT = "footerHeight";
  public static final String HEADER_HEIGHT = "headerHeight";
  public static final String HEADER = "header";
  public static final String HORIZONTAL_LINES = "horizontalLines";
  public static final String HORIZONTAL_SCROLL_BAR = "horizontalScrollBar";
  public static final String INITIAL_LIST_VIEW = "initialListView";
  public static final String SEARCH_ON_INPUT = "searchOnInput";
  public static final String SELECTION_MODE = "selectionMode";
  public static final String VERTICAL_LINES = "verticalLines";
  public static final String VERTICAL_SCROLL_BAR = "verticalScrollBar";
  public static final String ALLOW_FORM_CUSTOMIZE = "allowFormCustomize";
  public static final String AUTO_FILL_CHECK = "autoFillCheck";
  public static final String AUTO_SAVE_DATA_IN_SETTINGS = "autoSaveDataInSettings";
  public static final String AUTO_TITLE = "autoTitle";
  public static final String AUTO_URL = "autoUrl";
  public static final String CHILDREN_ALIGN = "childrenAlign";
  public static final String COLLAPSE_FORM_ITEMS_BY_IMPORTACE = "collapseFormItemsByImportance";
  public static final String COMMAND_BAR_LOCATION = "commandBarLocation";
  public static final String COMMAND_BAR = "commandBar";
  public static final String COMMAND_INTERFACE = "commandInterface";
  public static final String COMMAND = "command";
  public static final String CONVERSATIONS_REPRESENTATION = "conversationsRepresentation";
  public static final String CURRENT_ROW_USE = "currentRowUse";
  public static final String ENTER_KEY_BEHAVIOR = "enterKeyBehavior";
  public static final String GROUP = "group";
  public static final String HEIGHT = "height";
  public static final String HORIZONTAL_SPACING = "horizontalSpacing";
  public static final String HORIZONTAL_STRETCH = "horizontalStretch";
  public static final String NAVIGATION_PANEL = "navigationPanel";
  public static final String PAGES_REPRESENTATION = "pagesRepresentation";
  public static final String SAVE_DATA_IN_SETTINGS = "saveDataInSettings";
  public static final String SCALE = "scale";
  public static final String SCALING_MODE = "scalingMode";
  public static final String SETTINGS_STORAGE = "settingsStorage";
  public static final String SHOW_CLOSE_BUTTON = "showCloseButton";
  public static final String SHOW_TITLE = "showTitle";
  public static final String SOURCE = "source";
  public static final String TYPES = "types";
  public static final String USE_FOR_FOLDER_AND_ITEMS = "useForFolderAndItems";
  public static final String VALUE_TYPE = "valueType";
  public static final String VERTICAL_SCROLL = "verticalScroll";
  public static final String VERTICAL_SPACING = "verticalSpacing";
  public static final String VERTICAL_STRETCH = "verticalStretch";
  public static final String WIDTH = "width";
  public static final String WINDOW_OPENING_MODE = "windowOpeningMode";
  public static final String CONFIGURATION = "Configuration";
  public static final String CONTAINED_OBJECTS = "containedObjects";
  public static final String DEFAULT_ROLES = "defaultRoles";
  public static final String UPDATE_CATALOG_ADDRESS = "updateCatalogAddress";
  public static final String REPORTS_VARIANTS_STORAGE = "reportsVariantsStorage";
  public static final String DEFAULT_REPORT_FORM = "defaultReportForm";
  public static final String DEFAULT_REPORT_VARIANT_FORM = "defaultReportVariantForm";
  public static final String DEFAULT_REPORT_SETTINGS_FORM = "defaultReportSettingsForm";
  public static final String DEFAULT_SEARCH_FORM = "defaultSearchForm";
  public static final String MAIN_SECTION_PICTURE = "mainSectionPicture";
  public static final String SPLASH = "splash";
  public static final String COPYRIGHT = "copyright";
  public static final String VENDOR_INFORMATION_ADDRESS = "vendorInformationAddress";
  public static final String OBJECT_AUTO_NUMERATION_MODE = "objectAutonumerationMode";
  public static final String DEFAULT_STYLE = "defaultStyle";
  public static final String LANGUAGES = "languages";
  public static final String LANGUAGE_CODE = "languageCode";
  public static final String SUBSYSTEMS = "subsystems";
  public static final String STYLE_ITEMS = "styleItems";
  public static final String STYLES = "styles";
  public static final String COMMON_PICTURES = "commonPictures";
  public static final String CONFIGURATION_INFORMATION_ADDRESS = "configurationInformationAddress";
  public static final String INCLUDE_HELP_IN_CONTENTS = "includeHelpInContents";
  public static final String HELP = "help";
  public static final String PAGES = "pages";
  public static final String LANG = "lang";
  public static final String USE_MANAGED_FORM_IN_ORDINARY_APPLICATION = "useManagedFormInOrdinaryApplication";
  public static final String USED_MOBILE_APPLICATION_FUNCTIONALITIES = "usedMobileApplicationFunctionalities";
  public static final String FUNCTIONALITY = "functionality";
  public static final String USE = "use";
  public static final String INTERFACE_COMPATIBILITY_MODE = "interfaceCompatibilityMode";
  public static final String SESSION_PARAMETERS = "sessionParameters";
  public static final String ROLES = "roles";
  public static final String COMMON_TEMPLATES = "commonTemplates";
  public static final String CATALOGS = "catalogs";
  public static final String CATALOG = "Catalog";
  public static final String PRODUCED_TYPES = "producedTypes";
  public static final String OBJECT_TYPE = "objectType";
  public static final String SELECTION_TYPE = "selectionType";
  public static final String LIST_TYPE = "listType";
  public static final String MANAGER_TYPE = "managerType";
  public static final String REF_TYPE = "refType";
  public static final String USE_STANDARD_COMMANDS = "useStandardCommands";
  public static final String INPUT_BY_STRING = "inputByString";
  public static final String FULL_TEXT_SEARCH_ON_INPUT_BY_STRING = "fullTextSearchOnInputByString";
  public static final String STANDARD_ATTRIBUTES = "standardAttributes";
  public static final String DATA_HISTORY = "dataHistory";
  public static final String FILL_VALUE = "fillValue";
  public static final String FULL_TEXT_SEARCH = "fullTextSearch";
  public static final String MIN_VALUE = "minValue";
  public static final String MAX_VALUE = "maxValue";
  public static final String TOOL_TIP = "toolTip";
  public static final String FILL_FROM_FILLING_VALUE = "fillFromFillingValue";
  public static final String FILL_CHECKING = "fillChecking";
  public static final String CREATE_ON_INPUT = "createOnInput";
  public static final String OBJECT_PRESENTATION = "objectPresentation";
  public static final String EXPLANATION = "explanation";
  public static final String LEVEL_COUNT = "levelCount";
  public static final String FOLDERS_ON_TOP = "foldersOnTop";
  public static final String OWNERS = "owners";
  public static final String CODE_LENGTH = "codeLength";
  public static final String DESCRIPTION_LENGTH = "descriptionLength";
  public static final String CODE_TYPE = "codeType";
  public static final String CODE_ALLOWED_LENGTH = "codeAllowedLength";
  public static final String CHECK_UNIQUE = "checkUnique";
  public static final String AUTONUMBERING = "autonumbering";
  public static final String DEFAULT_PRESENTATION = "defaultPresentation";
  public static final String EDIT_TYPE = "editType";
  public static final String CHOICE_MODE = "choiceMode";
}
