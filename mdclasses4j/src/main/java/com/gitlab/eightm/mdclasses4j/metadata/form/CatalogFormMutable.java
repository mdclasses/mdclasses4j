/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.form;

import com.gitlab.eightm.mdclasses4j.metadata.form.ext.CatalogFormExtInfoMutable;
import com.gltlab.eightm.mdclasses.model.Help;
import com.gltlab.eightm.mdclasses.model.base.MultiLanguageContent;
import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.forms.CatalogForm;
import com.gltlab.eightm.mdclasses.model.forms.Event;
import com.gltlab.eightm.mdclasses.model.forms.EventHandler;
import com.gltlab.eightm.mdclasses.model.forms.FormCommandInterface;
import com.gltlab.eightm.mdclasses.model.forms.element.AutoCommandBar;
import com.gltlab.eightm.mdclasses.model.forms.element.FormAttribute;
import com.gltlab.eightm.mdclasses.model.forms.element.FormItem;
import com.gltlab.eightm.mdclasses.model.forms.ext.CatalogFormExtInfo;
import com.gltlab.eightm.mdclasses.model.v8enum.AutoSaveFormDataInSettings;
import com.gltlab.eightm.mdclasses.model.v8enum.CollapseFormItemsByImportance;
import com.gltlab.eightm.mdclasses.model.v8enum.CommandBarLocation;
import com.gltlab.eightm.mdclasses.model.v8enum.FormBaseFontVariant;
import com.gltlab.eightm.mdclasses.model.v8enum.FormChildrenAlign;
import com.gltlab.eightm.mdclasses.model.v8enum.FormChildrenGroup;
import com.gltlab.eightm.mdclasses.model.v8enum.FormConversationsRepresentation;
import com.gltlab.eightm.mdclasses.model.v8enum.FormEnterKeyBehavior;
import com.gltlab.eightm.mdclasses.model.v8enum.FormItemSpacing;
import com.gltlab.eightm.mdclasses.model.v8enum.FormType;
import com.gltlab.eightm.mdclasses.model.v8enum.FormWindowOpeningMode;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemHorizontalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.ItemVerticalAlignment;
import com.gltlab.eightm.mdclasses.model.v8enum.LogFormScrollMode;
import com.gltlab.eightm.mdclasses.model.v8enum.SaveFormDataInSettings;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class CatalogFormMutable implements CatalogForm {
  private MultiLanguageText title = new MultiLanguageTextMutable();
  private String comment = "";
  private String name = "";
  private boolean enabled;
  private List<MultiLanguageContent> synonym = new ArrayList<>();
  private String uuid;
  private FormEnterKeyBehavior enterKeyBehavior = FormEnterKeyBehavior.ELEMENT_NAVIGATION;
  private AutoSaveFormDataInSettings autoSaveFormDataInSettings = AutoSaveFormDataInSettings.DONT_USE;
  private SaveFormDataInSettings saveFormDataInSettings = SaveFormDataInSettings.DONT_USE;
  private int width;
  private int height;
  private FormType formType = FormType.MANAGED;
  private List<Event> formEvents = new ArrayList<>();
  private List<FormAttribute> attributes = new ArrayList<>();
  private List<FormItem> items = new ArrayList<>();
  private String helpContent = "";
  private boolean includeHelpInContents;
  private AutoCommandBar autoCommandBar;
  private CommandBarLocation commandBarLocation = CommandBarLocation.AUTO;
  private List<EventHandler> handlers = new ArrayList<>();
  private CatalogFormExtInfo extInfo = new CatalogFormExtInfoMutable();
  private FormChildrenGroup group = FormChildrenGroup.VERTICAL;
  private boolean showTitle;
  private FormWindowOpeningMode windowOpeningMode = FormWindowOpeningMode.INDEPENDENT;
  private boolean autoTitle;
  private boolean autoUrl;
  private boolean showCloseButton;
  private FormChildrenAlign childrenAlign = FormChildrenAlign.AUTO;
  private FormItemSpacing horizontalSpacing = FormItemSpacing.AUTO;
  private ItemHorizontalAlignment horizontalAlignment = ItemHorizontalAlignment.AUTO;
  private ItemVerticalAlignment verticalAlignment = ItemVerticalAlignment.Auto;
  private FormItemSpacing verticalSpacing = FormItemSpacing.AUTO;
  private boolean autoFillCheck;
  private boolean allowFormCustomize;
  private FormCommandInterface commandInterface = new FormCommandInterfaceMutable();
  private FormBaseFontVariant scalingMode = FormBaseFontVariant.AUTO;
  private double scale = 100.0;
  private LogFormScrollMode verticalScroll = LogFormScrollMode.AUTO;
  private String settingsStorage = "";
  private FormConversationsRepresentation conversationsRepresentation = FormConversationsRepresentation.AUTO;
  private CollapseFormItemsByImportance collapseItemsByImportanceVariant = CollapseFormItemsByImportance.AUTO;
  private Help help;
}
