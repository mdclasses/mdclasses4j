/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.form.ext;

import com.gltlab.eightm.mdclasses.model.forms.EventHandler;
import com.gltlab.eightm.mdclasses.model.forms.ext.LabelFieldExtInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Setter
@Getter
public class LabelFieldExtInfoMutable implements LabelFieldExtInfo {
  private List<EventHandler> handlers = new ArrayList<>();
  private boolean autoMaxHeight;
  private boolean autoMaxWidth;
  private boolean horizontalStretch;
  private boolean verticalStretch;
  private Map<String, String> format = new HashMap<>();
  private int height;
  private boolean hyperlink;
  private boolean markNegatives;
  private int maxHeight;
  private int maxWidth;
  private int minWidth;
  private boolean passwordMode;
  private int width;
}
