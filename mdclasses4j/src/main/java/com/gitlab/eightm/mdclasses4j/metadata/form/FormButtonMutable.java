/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j.metadata.form;

import com.gltlab.eightm.mdclasses.model.base.MultiLanguageText;
import com.gltlab.eightm.mdclasses.model.base.RoleBoolean;
import com.gltlab.eightm.mdclasses.model.forms.ExtendedTooltip;
import com.gltlab.eightm.mdclasses.model.forms.element.Button;
import com.gltlab.eightm.mdclasses.model.forms.path.GeneralDataPath;
import com.gltlab.eightm.mdclasses.model.v8enum.DisplayImportance;
import com.gltlab.eightm.mdclasses.model.v8enum.ElementTitleLocation;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FormButtonMutable implements Button {
  private String name;
  private boolean defaultItem;
  private boolean enabled;
  private boolean visible;
  private RoleBoolean userVisible;
  private boolean skipOnInput;
  private MultiLanguageText title;
  private ExtendedTooltip extendedTooltip;
  private GeneralDataPath dataPath;
  private String titleBackColor;
  private int titleHeight;
  private ElementTitleLocation titleLocation;
  private int id;
  private DisplayImportance displayImportance;
}
