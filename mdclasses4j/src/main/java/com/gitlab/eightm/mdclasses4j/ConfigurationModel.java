/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.gitlab.eightm.mdclasses4j;

import com.gitlab.eightm.mdclasses4j.internal.ConfigurationParser;
import com.gitlab.eightm.mdclasses4j.context.ConfigurationContext;
import com.gitlab.eightm.mdclasses4j.internal.MetadataIndex;
import com.gitlab.eightm.mdclasses4j.jaxb.ConfigurationRoot;
import com.gitlab.eightm.mdclasses4j.jaxb.CustomCharacterEscapeHandler;
import com.gltlab.eightm.mdclasses.model.Configuration;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import lombok.SneakyThrows;
import org.glassfish.jaxb.core.marshaller.DataWriter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

public class ConfigurationModel {

  private final ConfigurationContext configurationContext = applicationContext.getBean(ConfigurationContext.class);
  private static final AnnotationConfigApplicationContext applicationContext;

  static {
    applicationContext = new AnnotationConfigApplicationContext();
    applicationContext.scan( "com.gitlab.eightm.mdclasses4j");
    applicationContext.refresh();
  }

  /**
   * @param pathToProjectRoot путь к корневой папке 1С проекта, которая содержит папку "Configuration"
   * @return
   */
  public void initializeContext(@NotNull Path pathToProjectRoot){

    if (!Files.exists(pathToProjectRoot)) {
      // TODO log error
      return;
    }

    var parser = applicationContext.getBean(ConfigurationParser.class);
    parser.initialize(pathToProjectRoot);

    try {
      var configurationEntity = parser.parseConfigurationRoot(pathToProjectRoot);
      if (configurationEntity.isEmpty()) {
        return;
      }
      configurationContext.initializeContext(configurationEntity.get());

    } catch (JAXBException e) {
      // TODO log error
    }
  }

  public Optional<ConfigurationRoot> getConfigurationRootData() {
    return Optional.of(configurationContext.getRoot());
  }

  public void getDefaultRoles() {
    configurationContext.defaultRoles();
  }

  @SneakyThrows
  public void writeConfiguration(@NotNull Configuration configuration) {
    JAXBContext jaxbContext = (JAXBContext) applicationContext.getBean("jaxbContext");
    var marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

    StringWriter stringWriter = new StringWriter();
    PrintWriter printWriter = new PrintWriter(stringWriter);
    DataWriter dataWriter = new DataWriter(printWriter, "UTF-8", new CustomCharacterEscapeHandler());
    dataWriter.setIndentStep(" ");

    marshaller.marshal(configuration, dataWriter);
    Files.writeString(Path.of("conf.mdo"), stringWriter.toString());
  }
}
