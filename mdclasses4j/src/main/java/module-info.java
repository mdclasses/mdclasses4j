module com.gitlab.eightm.mdclasses4j {
  requires static lombok;
  requires static org.jetbrains.annotations;
  requires mdclasses4j.model;
  requires spring.context;
  requires spring.beans;
  requires jakarta.xml.bind;
  requires com.sun.xml.bind.core;

  exports com.gitlab.eightm.mdclasses4j;

  opens com.gitlab.eightm.mdclasses4j to spring.context;
  opens com.gitlab.eightm.mdclasses4j.internal to spring.context;
}