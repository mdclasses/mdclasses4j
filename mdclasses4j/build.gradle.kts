/*
 * Copyright (c) 2021. Viktor Gukov <zchokobo@gmail.com>
 * This file is a part of mdclasses4j
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

plugins {
    // Apply the java-library plugin for API and implementation separation.
    `java-library`
    id("de.jjohannes.extra-java-module-info") version "0.9"
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {

    compileOnly("org.jetbrains:annotations:23.0.0")

    implementation("org.projectlombok:lombok:1.18.22")
    implementation((project(":model")))
    implementation("org.springframework:spring-context:5.3.14")
    implementation("jakarta.xml.bind:jakarta.xml.bind-api:3.0.0")
    implementation("com.sun.xml.bind:jaxb-impl:3.0.0")
    annotationProcessor("org.projectlombok:lombok:1.18.22")
    // Use JUnit Jupiter for testing.
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.2")
    testImplementation("org.projectlombok:lombok:1.18.22")
    testImplementation("org.assertj:assertj-core:3.18.1")
    testAnnotationProcessor("org.projectlombok:lombok:1.18.22")
}

tasks.named<Test>("test") {
    // Use JUnit Platform for unit tests.
    useJUnitPlatform()
}
